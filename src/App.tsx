import { Navigate, Route, Routes } from "react-router-dom";
import TodoApp from "./todos/TodoApp";

function App() {
  return (
    <Routes>
      <Route path="todos/*" element={<TodoApp />}></Route>
      <Route path="*" element={<Navigate to="todos" />} />
    </Routes>
  );
}

export default App;
