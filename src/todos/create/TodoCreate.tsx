import React from "react";

type TodoCreateProps = {
  onAddTodo: (name: string) => void;
};

function TodoCreate({ onAddTodo }: TodoCreateProps) {
  function handleKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
    if (event.key === "Enter") {
      const name = event.currentTarget.value.trim();
      if (name !== "") {
        onAddTodo(name);
        event.currentTarget.value = "";
      }
    }
  }

  return (
    <input
      type="text"
      className="new-todo"
      placeholder="What needs to be done?"
      autoFocus={true}
      onKeyPress={handleKeyPress}
    />
  );
}

export default TodoCreate;
