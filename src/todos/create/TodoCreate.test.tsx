import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import TodoCreate from "./TodoCreate";

describe("TodoCreate component", () => {
  it("should render", () => {
    render(<TodoCreate onAddTodo={() => {}} />);
    const inputElement = screen.getByRole("textbox");
    expect(inputElement).toBeInTheDocument();
  });

  it("should notify parent component and reset input value when creating todo with valid title", () => {
    const handleAddTodoMock = jest.fn();
    render(<TodoCreate onAddTodo={handleAddTodoMock} />);

    const inputElement = screen.getByRole("textbox");
    userEvent.type(inputElement, "new todo{enter}");

    expect(handleAddTodoMock).toHaveBeenCalledTimes(1);
    expect(handleAddTodoMock).toHaveBeenNthCalledWith(1, "new todo");
    expect(inputElement).toHaveValue("");
  });

  it.each(["", "     "])(
    'should not notify parent component and not reset input value when creating todo with invalid title: "%s"',
    (name) => {
      const handleAddTodoMock = jest.fn();
      render(<TodoCreate onAddTodo={handleAddTodoMock} />);

      const inputElement = screen.getByRole("textbox");
      userEvent.type(inputElement, name + "{enter}");

      expect(handleAddTodoMock).not.toHaveBeenCalled();
      expect(inputElement).toHaveValue(name);
    }
  );
});
