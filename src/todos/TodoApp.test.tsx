import { render, screen, within } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import TodoApp from "./TodoApp";
import useTodo from "./hooks/useTodo";
import { BrowserRouter } from "react-router-dom";

jest.mock("./hooks/useTodo");

describe("TodoApp component", () => {
  const deleteAllCompletedTodosMock = jest.fn();
  const createNewTodoMock = jest.fn();
  const ToggleAllTodosMock = jest.fn();
  const updateTodoMock = jest.fn();
  const deleteTodoMock = jest.fn();

  it("should render", () => {
    (useTodo as jest.Mock).mockReturnValue({
      deleteAllCompletedTodos: deleteAllCompletedTodosMock,
      createNewTodo: createNewTodoMock,
      toggleAllTodos: ToggleAllTodosMock,
      updateTodo: updateTodoMock,
      deleteTodo: deleteTodoMock,
      todos: [],
      allChecked: false,
      activeTodos: 0,
    });

    const { asFragment } = render(<TodoApp />, { wrapper: BrowserRouter });
    const element = screen.getByRole("heading");
    expect(element).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });

  it("should not display main and footer when no todo in todo list", () => {
    (useTodo as jest.Mock).mockReturnValue({
      deleteAllCompletedTodos: deleteAllCompletedTodosMock,
      createNewTodo: createNewTodoMock,
      toggleAllTodos: ToggleAllTodosMock,
      updateTodo: updateTodoMock,
      deleteTodo: deleteTodoMock,
      todos: [],
      allChecked: false,
      activeTodos: 0,
    });

    render(<TodoApp />, { wrapper: BrowserRouter });

    const mainElement = screen.queryByRole("checkbox", {
      name: "Mark all as complete",
    });
    expect(mainElement).toBeNull();
    expect(mainElement).not.toBeInTheDocument();

    const footerElement = screen.queryByText("item left");
    expect(footerElement).toBeNull();
    expect(footerElement).not.toBeInTheDocument();
  });

  it("the Mark all as complete checkbox should be checked when all todos are checked", () => {
    (useTodo as jest.Mock).mockReturnValue({
      deleteAllCompletedTodos: deleteAllCompletedTodosMock,
      createNewTodo: createNewTodoMock,
      toggleAllTodos: ToggleAllTodosMock,
      updateTodo: updateTodoMock,
      deleteTodo: deleteTodoMock,
      todos: [
        {
          name: "new todo",
          completed: true,
          id: "id1",
        },
        {
          name: "new todo2",
          completed: true,
          id: "id2",
        },
      ],
      allChecked: true,
      activeTodos: 2,
    });

    render(<TodoApp />, { wrapper: BrowserRouter });

    const checkboxElements = screen.getAllByRole("checkbox");
    const todoCheckboxElements = checkboxElements.slice(1);
    expect(todoCheckboxElements[0]).toBeChecked();
    expect(todoCheckboxElements[1]).toBeChecked();

    const toggleAllElement = screen.getByRole("checkbox", {
      name: "Mark all as complete",
    });
    expect(toggleAllElement).toBeChecked();
  });

  it("the Mark all as complete checkbox should not be checked when not all todos are checked", () => {
    (useTodo as jest.Mock).mockReturnValue({
      deleteAllCompletedTodos: deleteAllCompletedTodosMock,
      createNewTodo: createNewTodoMock,
      toggleAllTodos: ToggleAllTodosMock,
      updateTodo: updateTodoMock,
      deleteTodo: deleteTodoMock,
      todos: [
        {
          name: "new todo",
          completed: true,
          id: "id1",
        },
        {
          name: "new todo2",
          completed: false,
          id: "id2",
        },
      ],
      allChecked: false,
      activeTodos: 1,
    });

    render(<TodoApp />, { wrapper: BrowserRouter });

    const checkboxElements = screen.getAllByRole("checkbox");
    const todoCheckboxElements = checkboxElements.slice(1);
    expect(todoCheckboxElements[0]).toBeChecked();
    expect(todoCheckboxElements[1]).not.toBeChecked();

    const toggleAllElement = screen.getByRole("checkbox", {
      name: "Mark all as complete",
    });
    expect(toggleAllElement).not.toBeChecked();
  });

  it("should notify hook when todo is created", async () => {
    (useTodo as jest.Mock).mockReturnValue({
      deleteAllCompletedTodos: deleteAllCompletedTodosMock,
      createNewTodo: createNewTodoMock,
      toggleAllTodos: ToggleAllTodosMock,
      updateTodo: updateTodoMock,
      deleteTodo: deleteTodoMock,
      todos: [],
      allChecked: false,
      activeTodos: 0,
    });

    render(<TodoApp />, { wrapper: BrowserRouter });

    userEvent.type(await screen.findByRole("textbox"), "new todo{enter}");
    expect(createNewTodoMock).toHaveBeenCalledTimes(1);
    expect(createNewTodoMock).toHaveBeenNthCalledWith(1, "new todo");
  });

  it("should notify hook when todo is updated", async () => {
    (useTodo as jest.Mock).mockReturnValue({
      deleteAllCompletedTodos: deleteAllCompletedTodosMock,
      createNewTodo: createNewTodoMock,
      toggleAllTodos: ToggleAllTodosMock,
      updateTodo: updateTodoMock,
      deleteTodo: deleteTodoMock,
      todos: [
        {
          name: "new todo",
          completed: true,
          id: "id1",
        },
        {
          name: "new todo2",
          completed: false,
          id: "id2",
        },
      ],
      allChecked: false,
      activeTodos: 1,
    });

    render(<TodoApp />, { wrapper: BrowserRouter });

    const listElement = await screen.findAllByRole("list");
    const listItemElements = await within(listElement[0]).findAllByRole(
      "listitem"
    );
    const checkboxElement = await within(listItemElements[0]).findByRole(
      "checkbox"
    );
    userEvent.click(checkboxElement);

    expect(updateTodoMock).toHaveBeenCalledTimes(1);
    expect(updateTodoMock).toHaveBeenNthCalledWith(
      1,
      { completed: false },
      "id1"
    );

    const labelElement = await within(listItemElements[0]).findByText(
      "new todo"
    );
    userEvent.dblClick(labelElement);

    const inputElement = await within(listItemElements[0]).findByRole(
      "textbox"
    );
    userEvent.type(inputElement, "{backspace}blabla{enter}");
    userEvent.click(inputElement);

    expect(updateTodoMock).toHaveBeenCalledTimes(2);
    expect(updateTodoMock).toHaveBeenNthCalledWith(
      2,
      { name: "new todblabla" },
      "id1"
    );
  });
});
