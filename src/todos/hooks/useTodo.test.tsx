import { act, renderHook } from "@testing-library/react-hooks";
import useTodo from "./useTodo";

describe("useTodo hook", () => {
  it("should allow todo creation, deletion and update", () => {
    const { result } = renderHook(() => useTodo([], false, 0));
    expect(result.current.activeTodos).toBe(0);
    expect(result.current.todos).toEqual([]);
    expect(result.current.allChecked).toBe(false);

    act(() => {
      result.current.createNewTodo("first todo");
    });

    expect(result.current.activeTodos).toBe(1);
    expect(result.current.todos).toEqual([
      { id: expect.any(String), name: "first todo", completed: false },
    ]);
    expect(result.current.allChecked).toBe(false);

    act(() => {
      result.current.createNewTodo("second todo");
    });

    expect(result.current.activeTodos).toBe(2);
    expect(result.current.todos[1]).toHaveProperty("name", "second todo");
    expect(result.current.todos[1]).toHaveProperty("completed", false);
    expect(result.current.allChecked).toBe(false);

    act(() => {
      result.current.updateTodo(
        { name: "second todo updated", completed: true },
        result.current.todos[1].id
      );
    });

    expect(result.current.activeTodos).toBe(1);
    expect(result.current.todos[1]).toHaveProperty(
      "name",
      "second todo updated"
    );
    expect(result.current.todos[1]).toHaveProperty("completed", true);
    expect(result.current.allChecked).toBe(false);

    act(() => {
      result.current.updateTodo(
        { name: "first todo updated", completed: true },
        result.current.todos[0].id
      );
    });

    expect(result.current.activeTodos).toBe(0);
    expect(result.current.todos[0]).toHaveProperty(
      "name",
      "first todo updated"
    );
    expect(result.current.todos[0]).toHaveProperty("completed", true);
    expect(result.current.allChecked).toBe(true);

    act(() => {
      result.current.deleteTodo(result.current.todos[0].id);
    });

    expect(result.current.activeTodos).toBe(0);
    expect(result.current.todos[0]).toHaveProperty(
      "name",
      "second todo updated"
    );
    expect(result.current.todos[0]).toHaveProperty("completed", true);
    expect(result.current.allChecked).toBe(true);
  });

  // Todo: Maybe add more tests here to test when they are not all true / false
  it("should allow to toggle and delete All Selected Todos", () => {
    const { result } = renderHook(() =>
      useTodo(
        [
          {
            name: "first todo",
            completed: false,
            id: "id1",
          },
          {
            name: "second todo",
            completed: false,
            id: "id2",
          },
          {
            name: "third todo",
            completed: false,
            id: "id3",
          },
        ],
        false,
        0
      )
    );
    expect(result.current.activeTodos).toBe(3);
    expect(result.current.todos).toEqual([
      {
        name: "first todo",
        completed: false,
        id: "id1",
      },
      {
        name: "second todo",
        completed: false,
        id: "id2",
      },
      {
        name: "third todo",
        completed: false,
        id: "id3",
      },
    ]);
    expect(result.current.allChecked).toBe(false);

    act(() => {
      result.current.toggleAllTodos();
    });

    expect(result.current.activeTodos).toBe(0);
    expect(result.current.todos).toEqual([
      {
        name: "first todo",
        completed: true,
        id: "id1",
      },
      {
        name: "second todo",
        completed: true,
        id: "id2",
      },
      {
        name: "third todo",
        completed: true,
        id: "id3",
      },
    ]);
    expect(result.current.allChecked).toBe(true);

    act(() => {
      result.current.deleteAllCompletedTodos();
    });

    expect(result.current.activeTodos).toBe(0);
    expect(result.current.todos).toEqual([]);
    expect(result.current.allChecked).toBe(false);
  });
});
