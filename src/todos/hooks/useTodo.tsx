import { useEffect, useState } from "react";
import { Todos } from "../Todo.model";

function useTodo(
  initialTodos: Todos,
  initialAllChecked: boolean,
  initialActiveTodos: number
) {
  const [todos, setTodos] = useState<Todos>(initialTodos);
  const [allChecked, setAllChecked] = useState(initialAllChecked);
  const [activeTodos, setActiveTodos] = useState(
    initialActiveTodos
  );

  useEffect(() => {
    setAllChecked(todos.length > 0 && todos.every((todo) => todo.completed));
    setActiveTodos(todos.filter((todo) => !todo.completed).length);
  }, [todos]);

  function updateTodo(
    updatedValue: { completed?: boolean; name?: string },
    id: string
  ) {
    setTodos((todos) => {
      return todos.map((todo) => {
        if (id === todo.id) {
          todo.completed = updatedValue.completed ?? todo.completed;
          todo.name = updatedValue.name ?? todo.name;
        }
        return todo;
      });
    });
  }

  function deleteTodo(id: string) {
    setTodos((todos) => {
      return todos.filter((todo) => todo.id !== id);
    });
  }

  function createNewTodo(name: string) {
    setTodos((todos) => {
      return todos.concat([
        {
          completed: false,
          name,
          id: generateUniqueId(name),
        },
      ]);
    });
  }

  function generateUniqueId(name: string): string {
    return `${name}_${new Date().getTime()}`;
  }

  function toggleAllTodos() {
    todos.forEach(({ id }) => {
      updateTodo({ completed: !allChecked }, id);
    });
  }

  function deleteAllCompletedTodos() {
    todos.forEach(({ id, completed }) => {
      if (completed) deleteTodo(id);
    });
  }

  return {
    todos,
    allChecked,
    activeTodos,
    createNewTodo,
    updateTodo,
    deleteTodo,
    toggleAllTodos,
    deleteAllCompletedTodos,
  };
}

export default useTodo;
