export interface Todo {
  completed: boolean;
  name: string;
  id: string;
}

export type Todos = Array<Todo>;
