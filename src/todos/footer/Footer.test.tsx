import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { BrowserRouter } from "react-router-dom";
import Footer from "./Footer";

describe("Footer component", () => {
  it("should render", () => {
    const { asFragment } = render(
      <BrowserRouter>
        <Footer
          activeTodos={2}
          hasCompletedTodos={false}
          onClearCompleted={() => {}}
        />
      </BrowserRouter>
    );
    const textElement = screen.getByText("items left");
    expect(textElement).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });

  it.each([0, 2])(
    "should render plural text when $i todos are active",
    (activeTodos) => {
      render(
        <BrowserRouter>
          <Footer
            activeTodos={activeTodos}
            hasCompletedTodos={false}
            onClearCompleted={() => {}}
          />
        </BrowserRouter>
      );
      // FIXME: expect(screen.getByRole("strong")).toHaveTextContent(activeTodos.toString());
      const textElement = screen.getByText("items left");
      expect(textElement).toBeInTheDocument();
    }
  );

  it("should render singular text when 1 todo is active", () => {
    render(
      <BrowserRouter>
        <Footer
          activeTodos={1}
          hasCompletedTodos={false}
          onClearCompleted={() => {}}
        />
      </BrowserRouter>
    );
    // FIXME: expect(screen.getByRole("strong")).toHaveTextContent("1");
    const textElement = screen.getByText("item left");
    expect(textElement).toBeInTheDocument();
  });

  it("should notify parent when Clear completed button is clicked", () => {
    const handleClearCompletedMock = jest.fn();

    render(
      <BrowserRouter>
        <Footer
          activeTodos={1}
          hasCompletedTodos={true}
          onClearCompleted={handleClearCompletedMock}
        />
      </BrowserRouter>
    );

    const buttonElement = screen.getByRole("button");
    userEvent.click(buttonElement);

    expect(handleClearCompletedMock).toHaveBeenCalledTimes(1);
  });
});
