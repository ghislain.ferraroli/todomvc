import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";

type FooterProps = {
  activeTodos: number;
  hasCompletedTodos: boolean;
  onClearCompleted: () => void;
};

function Footer({
  activeTodos,
  onClearCompleted,
  hasCompletedTodos,
}: FooterProps) {
  let location = useLocation();
  const [selected, setSelected] = useState<0 | 1 | 2>(0);

  useEffect(() => {
    switch (location.pathname) {
      case "/todos/active":
        setSelected(1);
        break;
      case "/todos/completed":
        setSelected(2);
        break;
      default:
        setSelected(0);
        break;
    }
  }, [location]);

  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{activeTodos}</strong>
        {` item${activeTodos !== 1 ? "s" : ""} left`}
      </span>

      <ul className="filters">
        <li>
          <Link to="" className={selected === 0 ? "selected" : ""}>
            All
          </Link>
        </li>
        <li>
          <Link to="active" className={selected === 1 ? "selected" : ""}>
            Active
          </Link>
        </li>
        <li>
          <Link to="completed" className={selected === 2 ? "selected" : ""}>
            Completed
          </Link>
        </li>
      </ul>

      {hasCompletedTodos && (
        <button className="clear-completed" onClick={onClearCompleted}>
          Clear completed
        </button>
      )}
    </footer>
  );
}

export default Footer;
