import { render, screen } from "@testing-library/react";
import TodoList from "./TodoList";
import userEvent from "@testing-library/user-event";

describe("TodoList component", () => {
  it("should render", () => {
    const { asFragment } = render(
      <TodoList
        onUpdate={() => {}}
        onDestroy={() => {}}
        todos={[
          {
            completed: false,
            name: "test",
            id: "123test",
          },
        ]}
        filter={0}
      />
    );
    const element = screen.getByRole("list");
    expect(element).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });

  it("should notify parent on destroy button click", () => {
    const handleDestroyMock = jest.fn();

    render(
      <TodoList
        onUpdate={() => {}}
        onDestroy={handleDestroyMock}
        todos={[
          {
            completed: false,
            name: "test",
            id: "123test",
          },
        ]}
        filter={0}
      />
    );

    userEvent.click(screen.getByRole("button"));

    expect(handleDestroyMock).toHaveBeenCalledTimes(1);
    expect(handleDestroyMock).toHaveBeenNthCalledWith(1, "123test");
  });

  it.each`
    completed | expected
    ${false}  | ${true}
    ${true}   | ${false}
  `(
    "should notify parent on checkbox click when completed is $completed",
    ({ completed, expected }) => {
      const handleStatusChangeMock = jest.fn();

      render(
        <TodoList
          onUpdate={handleStatusChangeMock}
          onDestroy={() => {}}
          todos={[
            {
              completed: completed,
              name: "test",
              id: "123test",
            },
          ]}
          filter={0}
        />
      );

      userEvent.click(screen.getByRole("checkbox"));

      expect(handleStatusChangeMock).toHaveBeenCalledTimes(1);
      expect(handleStatusChangeMock).toHaveBeenNthCalledWith(
        1,
        { completed: expected },
        "123test"
      );
    }
  );

  it.each`
    filter | expectedLength
    ${0}   | ${3}
    ${1}   | ${2}
    ${2}   | ${1}
  `(
    "should display $expectedLength todos when using filter $filter",
    async ({ filter, expectedLength }) => {
      const handleStatusChangeMock = jest.fn();

      render(
        <TodoList
          onUpdate={handleStatusChangeMock}
          onDestroy={() => {}}
          todos={[
            {
              name: "first todo",
              completed: false,
              id: "todo1",
            },
            {
              name: "second todo",
              completed: true,
              id: "todo2",
            },
            {
              name: "third todo",
              completed: false,
              id: "todo3",
            },
          ]}
          filter={filter}
        />
      );

      const listItemElements = await screen.findAllByRole("listitem");
      expect(listItemElements).toHaveLength(expectedLength);
    }
  );
});
