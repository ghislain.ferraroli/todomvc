import React, { useEffect, useRef, useState } from "react";
import { Todo } from "../../Todo.model";

type TodoItemProps = Omit<Todo, "id"> & {
  onUpdate: (updatedValue: { completed?: boolean; name?: string }) => void;
  onDestroy: () => void;
};

function TodoItem({ completed, name, onUpdate, onDestroy }: TodoItemProps) {
  const [isEditing, setIsEditing] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (inputRef.current && isEditing) {
      inputRef.current.focus();
      inputRef.current.value = name;
    }
    // TODO: This useEffect triggers a potential leak warning, how can I prevent it ?
    // I tried the following, it removes the warning, but breaks the behavior of the app:
    // return () => {
    //   stopEdit();
    // };
  }, [isEditing, name]);

  function handleUpdate(updatedValue: { name: string }) {
    onUpdate(updatedValue);
    stopEdit();
  }

  function startEdit() {
    setIsEditing(true);
    document.addEventListener("keydown", handleKeyDown);
  }

  function handleKeyDown(event: KeyboardEvent) {
    if (event.key === "Escape") {
      stopEdit();
    }
  }

  function handleBlur(name: string) {
    onUpdate({ name });
    stopEdit();
  }

  function stopEdit() {
    document.removeEventListener("keydown", handleKeyDown);
    setIsEditing(false);
  }

  function handleInputKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
    if (event.key === "Enter") {
      const name = event.currentTarget.value.trim();
      if (name === "") {
        onDestroy();
      } else {
        handleUpdate({ name });
        event.currentTarget.value = "";
      }
    }
  }

  return (
    <li
      className={`${isEditing ? "editing" : ""} ${
        completed ? "completed" : ""
      }`}
    >
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={completed}
          onChange={(event) => onUpdate({ completed: event.target.checked })}
        />
        <label onDoubleClick={() => startEdit()}>{name}</label>
        <button className="destroy" onClick={onDestroy}></button>
      </div>
      <input
        type="text"
        className="edit"
        ref={inputRef}
        onKeyPress={handleInputKeyPress}
        onBlur={(event) => handleBlur(event.target.value)}
      ></input>
    </li>
  );
}

export default TodoItem;
