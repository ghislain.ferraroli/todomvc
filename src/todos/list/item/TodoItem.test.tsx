import { render, screen } from "@testing-library/react";
import TodoItem from "./TodoItem";
import userEvent from "@testing-library/user-event";

describe("TodoItem component", () => {
  it("should render", () => {
    const { asFragment } = render(
      <TodoItem
        completed={false}
        name={"Todo name"}
        onUpdate={() => {}}
        onDestroy={() => {}}
      />
    );

    const element = screen.getByText("Todo name");
    expect(element).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });

  it("should notify parent on destroy button click", () => {
    const handleDestroyMock = jest.fn();

    render(
      <TodoItem
        completed={true}
        name={"test"}
        onUpdate={() => {}}
        onDestroy={() => handleDestroyMock()}
      />
    );

    userEvent.click(screen.getByRole("button"));

    expect(handleDestroyMock).toHaveBeenCalledTimes(1);
    expect(handleDestroyMock).toHaveBeenNthCalledWith(1);
  });

  it.each`
    completed | expected
    ${false}  | ${true}
    ${true}   | ${false}
  `(
    "should notify parent on checkbox click when completed is $completed",
    ({ completed, expected }) => {
      const handleStatusChangeMock = jest.fn();

      render(
        <TodoItem
          completed={completed}
          name={"test"}
          onUpdate={handleStatusChangeMock}
          onDestroy={() => {}}
        />
      );

      userEvent.click(screen.getByRole("checkbox"));

      expect(handleStatusChangeMock).toHaveBeenCalledTimes(1);
      expect(handleStatusChangeMock).toHaveBeenNthCalledWith(1, {
        completed: expected,
      });
    }
  );

  it("checkbox should have a checked attribute when completed is true", () => {
    render(
      <TodoItem
        completed={true}
        name={"test"}
        onUpdate={() => {}}
        onDestroy={() => {}}
      />
    );

    const element = screen.getByRole("checkbox");
    expect(element).toBeChecked();
  });

  it("checkbox should not have a checked attribute when completed is false", () => {
    render(
      <TodoItem
        completed={false}
        name={"test"}
        onUpdate={() => {}}
        onDestroy={() => {}}
      />
    );

    const element = screen.getByRole("checkbox");
    expect(element).not.toBeChecked();
  });

  it("li should have a completed class when completed is true", () => {
    render(
      <TodoItem
        completed={true}
        name={"test"}
        onUpdate={() => {}}
        onDestroy={() => {}}
      />
    );

    const element = screen.getByRole("listitem");
    expect(element).toHaveClass("completed");
  });

  it("li should not have a completed class when completed is false", () => {
    render(
      <TodoItem
        completed={false}
        name={"test"}
        onUpdate={() => {}}
        onDestroy={() => {}}
      />
    );

    const element = screen.getByRole("listitem");
    expect(element).not.toHaveClass("completed");
  });

  it("input should be in edit mode when double click on label", () => {
    render(
      <TodoItem
        completed={false}
        name={"Test name"}
        onUpdate={() => {}}
        onDestroy={() => {}}
      />
    );

    const labelElement = screen.getByText("Test name");
    userEvent.dblClick(labelElement);

    const listElement = screen.getByRole("listitem");
    expect(listElement).toHaveClass("editing");
  });

  it("should change todo name when user edit it and press enter", () => {
    const handleUpdateMock = jest.fn();

    render(
      <TodoItem
        completed={false}
        name={"Test name"}
        onUpdate={handleUpdateMock}
        onDestroy={() => {}}
      />
    );

    const labelElement = screen.getByText("Test name");
    userEvent.dblClick(labelElement);
    const inputElement = screen.getByRole("textbox");
    userEvent.type(inputElement, "{backspace}blabla{enter}");

    expect(handleUpdateMock).toHaveBeenNthCalledWith(1, {
      name: "Test namblabla",
    });
  });

  it("should change todo name when user edit it and click outside of the input", () => {
    const handleUpdateMock = jest.fn();

    render(
      <div>
        <button data-testid="test-button"></button>
        <TodoItem
          completed={false}
          name={"Test name"}
          onUpdate={handleUpdateMock}
          onDestroy={() => {}}
        />
      </div>
    );

    const LabelElement = screen.getByText("Test name");
    userEvent.dblClick(LabelElement);
    const inputElement = screen.getByRole("textbox");
    userEvent.type(inputElement, "{backspace}blabla");
    const buttonElement = screen.getByTestId("test-button");
    userEvent.click(buttonElement);

    expect(handleUpdateMock).toHaveBeenNthCalledWith(1, {
      name: "Test namblabla",
    });
  });

  it("input should cancel edit mode when user press escape", () => {
    const handleUpdateMock = jest.fn();

    render(
      <TodoItem
        completed={false}
        name={"Test name"}
        onUpdate={handleUpdateMock}
        onDestroy={() => {}}
      />
    );

    const LabelElement = screen.getByText("Test name");
    userEvent.dblClick(LabelElement);

    const inputElement = screen.getByRole("textbox");
    userEvent.type(inputElement, "{backspace}blabla");

    userEvent.keyboard("{Escape}");

    expect(LabelElement).toHaveTextContent("Test name");
    expect(handleUpdateMock).not.toHaveBeenCalled();
  });

  it.each(["", "    "])(
    'should delete todo when user edit name and saves with: "%s"',
    (newName) => {
      const handleUpdateMock = jest.fn();
      const handleDestroyMock = jest.fn();

      render(
        <TodoItem
          completed={false}
          name={"Test"}
          onUpdate={handleUpdateMock}
          onDestroy={handleDestroyMock}
        />
      );

      const LabelElement = screen.getByText("Test");
      userEvent.dblClick(LabelElement);

      const inputElement = screen.getByRole("textbox");
      userEvent.clear(inputElement);
      userEvent.type(inputElement, newName + "{enter}");

      expect(LabelElement).toHaveTextContent("Test");
      expect(handleUpdateMock).not.toHaveBeenCalled();
      expect(handleDestroyMock).toHaveBeenCalledTimes(1);
    }
  );
});
