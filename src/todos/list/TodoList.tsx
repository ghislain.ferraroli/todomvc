import { Todos } from "../Todo.model";
import TodoItem from "./item/TodoItem";

type TodoListProps = {
  todos: Todos;
  filter: 0 | 1 | 2;
  onUpdate: (
    updatedValue: { completed?: boolean; name?: string },
    id: string
  ) => void;
  onDestroy: (id: string) => void;
};

function TodoList({ todos, filter, onUpdate, onDestroy }: TodoListProps) {
  return (
    <ul className="todo-list">
      {todos
        .filter((todo) => {
          switch (filter) {
            case 1:
              return !todo.completed;
            case 2:
              return todo.completed;
            default:
              return true;
          }
        })
        .map(({ completed, name, id }) => {
          return (
            <TodoItem
              key={id}
              completed={completed}
              name={name}
              onUpdate={(updatedValue) => onUpdate(updatedValue, id)}
              onDestroy={() => onDestroy(id)}
            />
          );
        })}
    </ul>
  );
}

export default TodoList;
