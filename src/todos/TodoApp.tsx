import Footer from "./footer/Footer";
import TodoCreate from "./create/TodoCreate";
import TodoList from "./list/TodoList";
import useTodo from "./hooks/useTodo";
import { Route, Routes } from "react-router-dom";

function TodoApp() {
  const {
    todos,
    allChecked,
    activeTodos,
    deleteAllCompletedTodos,
    createNewTodo,
    toggleAllTodos,
    updateTodo,
    deleteTodo,
  } = useTodo([], false, 0);

  const todoList = (filter: 0 | 1 | 2) => {
    return (
      <TodoList
        todos={todos}
        onUpdate={(updatedValue, id) => updateTodo(updatedValue, id)}
        onDestroy={(id) => deleteTodo(id)}
        filter={filter}
      />
    );
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>Todos</h1>
        <TodoCreate onAddTodo={createNewTodo} />
      </header>
      {todos.length > 0 && (
        <>
          <section className="main">
            <input
              id="toggle-all"
              className="toggle-all"
              type="checkbox"
              checked={allChecked}
              onChange={() => toggleAllTodos()}
            />
            <label htmlFor="toggle-all">Mark all as complete</label>

            <Routes>
              <Route path="/" element={todoList(0)} key={0} />
              <Route path="/active" element={todoList(1)} key={1} />
              <Route path="/completed" element={todoList(2)} key={2} />
            </Routes>
          </section>
          <Footer
            activeTodos={activeTodos}
            hasCompletedTodos={todos.length - activeTodos > 0}
            onClearCompleted={deleteAllCompletedTodos}
          />
        </>
      )}
    </section>
  );
}

export default TodoApp;
